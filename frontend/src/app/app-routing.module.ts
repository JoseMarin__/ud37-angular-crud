import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsEmployeeComponent } from './Employee/crud/details-employee/details-employee.component';
import { CreateEmployeeComponent } from './Employee/crud/create-employee/create-employee.component';
import { ListEmployeeComponent } from './Employee/crud/list-employee/list-employee.component';
import { UpdateEmployeeComponent } from './Employee/crud/update-employee/update-employee.component';

const routes: Routes = [
{ path: '', redirectTo: 'employee', pathMatch: 'full' },
{ path: 'employees', component: ListEmployeeComponent },
{ path: 'add', component: CreateEmployeeComponent },
{ path: 'update/:id', component: UpdateEmployeeComponent },
{ path: 'details/:id', component: DetailsEmployeeComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
