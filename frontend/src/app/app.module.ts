import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetailsEmployeeComponent } from './Employee/crud/details-employee/details-employee.component';
import { CreateEmployeeComponent } from './Employee/crud/create-employee/create-employee.component';
import { ListEmployeeComponent } from './Employee/crud/list-employee/list-employee.component';
import { UpdateEmployeeComponent } from './Employee/crud/update-employee/update-employee.component';


@NgModule({
  declarations: [
    AppComponent,
    DetailsEmployeeComponent,
    CreateEmployeeComponent,
    ListEmployeeComponent,
    UpdateEmployeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,      //necesario para conectar con API
    HttpClientModule  //necesario para conectar con API
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
