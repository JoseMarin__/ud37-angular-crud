DROP table IF EXISTS employee;

create table employee(
	id int auto_increment,
	firstname varchar(250),
	lastname varchar(250),
	emailid varchar(250)
);

insert into employee (firstname, lastname,emailid)values('Jose','Marin','josemarin@gmail.com');
insert into employee (firstname, lastname,emailid)values('Luis','Lopez','luilopez@gmail.com');
insert into employee (firstname, lastname,emailid)values('Joel','Ferrer','joelferrer@gmail.com');
insert into employee (firstname, lastname,emailid)values('Elli','Lupiaes','ellilupiaez@gmail.com');
insert into employee (firstname, lastname,emailid)values('Andrea','Monzo','andreamonzo@gmail.com');