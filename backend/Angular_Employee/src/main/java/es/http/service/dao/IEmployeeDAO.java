package es.http.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import es.http.service.dto.Employee;

/**
 * @author Jose
 *
 */
public interface IEmployeeDAO extends JpaRepository<Employee, Long>{
	

}
