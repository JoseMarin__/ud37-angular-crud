package es.http.service.service;

import java.util.List;

import es.http.service.dto.Employee;

public interface IEmployeeService {

	//Metodos del CRUD
	public List<Employee> listarEmployees();  
	
	public Employee guardarEmployee(Employee employee);	
	
	public Employee employeeXID(Long id); 
	
	public Employee actualizarEmployee(Employee employee); //Actualiza datos del cliente UPDATE
	
	public void eliminarEmployee(Long id);// Elimina el cliente DELETE
	
	
}
