package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IEmployeeDAO;
import es.http.service.dto.Employee;

@Service
public class EmployeeServiceImpl implements IEmployeeService{
	
	//Utilizamos los metodos de la interface IClienteDAO, es como si instaciaramos.
	@Autowired
	IEmployeeDAO iEmployeeDAO;
	
	@Override
	public List<Employee> listarEmployees() {
		
		return iEmployeeDAO.findAll();
	}

	@Override
	public Employee guardarEmployee(Employee employee) {
		
		return iEmployeeDAO.save(employee);
	}

	@Override
	public Employee employeeXID(Long id) {
		
		return iEmployeeDAO.findById(id).get();
	}
	

	@Override
	public Employee actualizarEmployee(Employee employee) {
		
		return iEmployeeDAO.save(employee);
	}

	@Override
	public void eliminarEmployee(Long id) {
		
		iEmployeeDAO.deleteById(id);
		
	}


}
