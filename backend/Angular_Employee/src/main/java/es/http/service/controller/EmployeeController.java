package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Employee;
import es.http.service.service.EmployeeServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class EmployeeController {
	
	@Autowired
	EmployeeServiceImpl employeeServiceImpl;
	
	@GetMapping("/employees")
	public List<Employee> listarEmployees(){
		return employeeServiceImpl.listarEmployees();
	}
	
	
	@PostMapping("/employees")
	public Employee salvarEmployee(@RequestBody Employee employee) {
		
		return employeeServiceImpl.guardarEmployee(employee);
	}
	
	
	@GetMapping("/employees/{id}")
	public Employee employeeXID(@PathVariable(name="id") Long id) {
		
		Employee employee_xid= new Employee();
		
		employee_xid=employeeServiceImpl.employeeXID(id);
		
		System.out.println("Employee XID: "+employee_xid);
		
		return employee_xid;
	}
	
	@PutMapping("/employees/{id}")
	public Employee actualizarEmployee(@PathVariable(name="id")Long id,@RequestBody Employee employee) {
		
		Employee employee_seleccionado= new Employee();
		Employee employee_actualizado= new Employee();
		
		employee_seleccionado= employeeServiceImpl.employeeXID(id);
		
		employee_seleccionado.setFirstname(employee.getFirstname());
		employee_seleccionado.setLastname(employee.getLastname());
		employee_seleccionado.setEmailid(employee.getEmailid());
		
		employee_actualizado = employeeServiceImpl.actualizarEmployee(employee_seleccionado);
		
		System.out.println("El employee actualizado es: "+ employee_actualizado);
		
		return employee_actualizado;
	}
	
	@DeleteMapping("/employees/{id}")
	public void eleiminarEmployee(@PathVariable(name="id")Long id) {
		employeeServiceImpl.eliminarEmployee(id);
	}
	
	
}
