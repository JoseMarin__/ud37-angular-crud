package es.http.service.dto;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employee")//en caso que la tabala sea diferente
public class Employee {

	//Atributos de entidad cliente
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	private Long id;
	@Column(name = "firstname")//no hace falta si se llama igual
	private String firstname;
	@Column(name = "lastname")//no hace falta si se llama igual
	private String lastname;
	@Column(name = "emailid")//no hace falta si se llama igual
	private String emailid;
	
	/**
	 * 
	 */
	public Employee() {
		super();
	}
	
	/**
	 * @param id
	 * @param firstname
	 * @param lastname
	 * @param emailid
	 */
	public Employee(Long id, String firstname, String lastname, String emailid) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.emailid = emailid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", emailid=" + emailid
				+ "]";
	}
	
	
	
	
	
	
	
	
	
	
	
}
